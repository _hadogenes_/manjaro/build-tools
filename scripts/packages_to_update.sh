#!/bin/bash -ex

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
. "$SCRIPT_DIR/common_functions.sh"


repo_list="$1"

packagesToUpdate=()
for projectName in *; do
    [ -f "$projectName/PKGBUILD" ] || continue
    pushd "$projectName" > /dev/null

    packageNames=( $(namesFromPkgbuild) )
    packageNameFirst="${packageNames[0]}"
    packageVersion=$(versionFromPkgbuild "$projectName")

    if ! grep -q "^$packageNameFirst-$packageVersion-\(any\|x86_64\).pkg.tar" "$repo_list"; then
        packagesToUpdate+=( "$packageNameFirst" )
    fi

    popd > /dev/null
done

echo "${packagesToUpdate[@]}"
