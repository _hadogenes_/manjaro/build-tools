#!/bin/bash

import os
import stat
import sys
import subprocess
import argparse
import shutil
from shlex import quote
import json
from pathlib import Path
import requests
import urllib.parse
import tempfile
from multiprocessing.pool import ThreadPool

import git

ARCHWEB_URL = "https://archlinux.org/packages/search/json/?q={}"
MANJARO_GITLAB_GROUP_LIST_URL = "https://gitlab.manjaro.org/api/v4/groups?page={}"
MANJARO_GITLAB_PROJECT_URL = "https://gitlab.manjaro.org/api/v4/projects/{}"
GITLAB_PROJECT_URL = "https://gitlab.com/api/v4/projects/{}"

ARCH_GIT_URL="https://github.com/archlinux/{}.git"

THREAD_POOL_NUM = os.cpu_count()
THREAD_POO_GITL_NUM = min(os.cpu_count(), 4)

class Pacman:
    @staticmethod
    def _call(flags, pkgs = [], eflgs = []):
        # Subprocess wrapper, get all data
        if not pkgs:
            cmd = ["pacman", "--noconfirm", flags]
        elif type(pkgs) == list:
            cmd = ["pacman", "--noconfirm", flags]
            cmd += [quote(s) for s in pkgs]
        else:
            cmd = ["pacman", "--noconfirm", flags, pkgs]

        if eflgs and any(eflgs):
            eflgs = [ x for x in eflgs if x ]
            cmd += eflgs

        pacmanEnv = os.environ.copy()
        pacmanEnv['LC_ALL'] = "C"

        p = subprocess.Popen(
            cmd,
            stderr = subprocess.PIPE,
            stdout = subprocess.PIPE,
            env = pacmanEnv
        )
        data = p.communicate()

        return {
            "code": p.returncode,
            "stdout": data[0].decode(),
            "stderr": data[1].rstrip(b'\n').decode()
        }

    @staticmethod
    def is_installed(package):
        # Return True if the specified package is installed
        return Pacman._call("-Q", package)["code"] == 0

    @staticmethod
    def get_info(package):
        # Get package information from database
        s = Pacman._call("-Qi" if Pacman.is_installed(package) else "-Si", package)
        if s["code"] != 0:
            raise RuntimeError("Failed to get info: {0}".format(s["stderr"]))

        interim = []
        for line in s["stdout"].split('\n'):
            if not line.split():
                continue

            if ':' in line:
                key, value = line.split(':', 1)
                interim.append([ key.strip(), value.strip() ])
            else:
                interim[-1][1] += " " + line.strip()

        result = {}
        for key, value in interim:
            if key not in result:
                result[key] = value

        return result

class PkgBuild:
    _SINGLE_VALUE = [ "pkgbase", "pkgver", "pkgrel", "epoch" ]

    def __init__(self, pkgPath):
        self._path = pkgPath
        self._cache = {}

    def _readValues(self):
        self._cache.clear()
        neededArgs = PkgBuild._SINGLE_VALUE + [ "pkgname" ]

        cmd = ""
        for arg in neededArgs:
            cmd += "echo 'echo ${}';".format(arg)

        with open(self._path, "r") as f:
            p = subprocess.run(
                args = [ "(cat; echo;" + cmd + ") | bash" ],
                shell = True,
                capture_output = True,
                stdin = f
            )
            values = p.stdout.decode().split("\n")

        for key, value in zip(neededArgs, values):
            if key in PkgBuild._SINGLE_VALUE:
                self._cache[key] = value
            else:
                self._cache[key] = value.split()

        for key in PkgBuild._SINGLE_VALUE:
            if key not in self._cache:
                self._cache[key] = None

    def name(self):
        if not self._cache:
            self._readValues()

        return self._cache['pkgname']

    def base(self):
        if not self._cache:
            self._readValues()

        return self._cache['pkgbase']

    def version(self):
        projectName = self.base()

        if projectName.endswith("-git") or projectName.endswith("-hg"):
            subprocess.run([ "makepkg", "--skippgpcheck", "--nodeps", "--verifysource" ], capture_output = True)
            subprocess.run([ "makepkg", "--skippgpcheck", "--nodeps", "--nobuild" ], capture_output = True) # Automatically updates PKGBUILD with new version

            for dirPath in self._path.parent.iterdir():
                if dirPath.is_dir():
                    if dirPath.name not in (".git", ".hg"):
                        shutil.rmtree(dirPath)

            self._readValues()

        try:
            pkgver = self._cache['pkgver']
            pkgrel = self._cache['pkgrel']
            pkgepoch = self._cache['epoch']

            pkgFullver = pkgver + "-" + pkgrel

            if pkgepoch:
                pkgFullver = pkgepoch + ":" + pkgFullver

            return pkgFullver
        except:
            return None

def jsonRequest(url):
    r = requests.get(url)
    return json.loads(r.text)

def applyJob(args):
    return args[0](*args[1:])

class Package:
    _GIT_CMD = git.cmd.Git()
    _MANJARO_GROUP_LIST = []
    _THREAD_POOLS = []

    @staticmethod
    def initThreadPools(thredPollNum):
        for _ in range(thredPollNum):
            Package._THREAD_POOLS.append(ThreadPool(THREAD_POO_GITL_NUM))

    @staticmethod
    def genManjaroGroupList(pool):
        print("Generating Manjaro gitlab group list")
        Package._MANJARO_GROUP_LIST[:] = []

        i = 1
        while True:
            groupListJson = jsonRequest(MANJARO_GITLAB_GROUP_LIST_URL.format(i))
            if not groupListJson:
                break

            for gitlabGroup in groupListJson:
                gitlabName = gitlabGroup['full_path']
                gitlabBaseName = gitlabName.split('/')[0]

                if gitlabBaseName in ("packages", "security-overlay"):
                    Package._MANJARO_GROUP_LIST.append(gitlabName)
        #        elif gitlabBaseName not in ("manjaro-arm"):
        #            groupList.append(gitlabName)

            i += 1

    @staticmethod
    def genPackateList(packageNameList, pool, ignorePkgrel):
        packages = []
        packageBaseSet = set()

        for packageName, archWebResult in pool.imap_unordered(lambda packageName: (packageName, jsonRequest(ARCHWEB_URL.format(urllib.parse.quote(packageName)))),
                                                                 packageNameList):
            packageBaseList = [ packageResult['pkgbase'] for packageResult in archWebResult['results'] if packageResult['pkgname'] == packageName ]

            packageBaseName = None
            if packageBaseList:
                packageBaseName = packageBaseList[0]
            else:
                packageBaseName = packageName

            if packageBaseName in packageBaseSet:
                continue

            packageBaseSet.add(packageBaseName)
            packages.append(Package(packageName, packageBaseName, ignorePkgrel))

        return packages

    def __init__(self, packageName, packageBaseName, ignorePkgrel):
        self._info = {
            'base': packageBaseName,
            'name': packageName,
            'version': Pacman.get_info(packageName)['Version']
        }
        self._ignorePkgrel = ignorePkgrel
        self._gitList = []

    def info(self):
        return self._info

    @property
    def base(self):
        return self._info['base']

    @property
    def name(self):
        return self._info['name']

    @property
    def version(self):
        return self._info['version']

    def _checkoutToVersion(self, gitRepo):
        result  = (None, None)
        gitverWoRel = (None, None)

        for _ in range(20):
            gitPath = Path(gitRepo.working_tree_dir)
            pkgbuildPath = None

            if gitPath.joinpath("trunk", "PKGBUILD").exists():
                pkgbuildPath = gitPath.joinpath("trunk", "PKGBUILD")
            elif gitPath.joinpath("PKGBUILD").exists():
                pkgbuildPath = gitPath.joinpath("PKGBUILD")
            else:
                print("WARNING: PKGBUILD not found")
                continue

            gitver = PkgBuild(pkgbuildPath).version()
            if not gitver:
                continue

            if self._ignorePkgrel and gitverWoRel == (None, None):
                versionStrip = "-".join(self.version.split("-")[:-1])
                gitverStrip = "-".join(gitver.split("-")[:-1])

                if versionStrip == gitverStrip:
                    currHash = list(gitRepo.iter_commits(max_count = 1))[-1].hexsha
                    gitverWoRel = (gitver, currHash)

            verCheckOut = subprocess.run([ "vercmp", self.version, gitver ], capture_output = True)
            verCheck = int(verCheckOut.stdout)

            if verCheck > 0:
                if result == (None, None):
                    result = gitverWoRel
                break
            elif verCheck == 0:
                currHash = list(gitRepo.iter_commits(max_count = 1))[-1].hexsha
                result = (gitver, currHash)

            prevHash = list(gitRepo.iter_commits(max_count = 2))[-1].hexsha

            gitRepo.git.checkout(prevHash)

        return result



    def _gitlabCheck(self, baseUrl, groupBaseName):
        groupName = urllib.parse.quote("/".join([ groupBaseName, self.base ]), safe = '')

        projectJson = jsonRequest(baseUrl.format(groupName))
        if "http_url_to_repo" in projectJson:
            return {
                'url': projectJson['http_url_to_repo']
            }

        return None

    def _archGitCheck(self, archGitRepo):
        archGitBranch="packages/" + self.base
        archGitUrl = ARCH_GIT_URL.format(urllib.parse.quote(archGitRepo))

        if Package._GIT_CMD.ls_remote(archGitUrl, archGitBranch):
            return {
                'url': archGitUrl,
                'branch': archGitBranch
            }

        return None

    def _genGitList(self, pool):
        manjaroJoblist = ( [ self._gitlabCheck, MANJARO_GITLAB_PROJECT_URL, gitlabName ] for gitlabName in Package._MANJARO_GROUP_LIST )
        archJoblist = ( [ self._archGitCheck, archGitRepo ] for archGitRepo in [ "svntogit-packages", "svntogit-community" ] )
        otherJoblist = []

        if "FALLBACK_GITLAB_GROUP" in os.environ:
            otherJoblist += [ [ self._gitlabCheck, GITLAB_PROJECT_URL, os.environ['FALLBACK_GITLAB_GROUP'] ] ]

        for gitInfo in pool.imap(applyJob, manjaroJoblist):
            if gitInfo:
                self._gitList.append(gitInfo)

        for gitInfo in pool.imap(applyJob, archJoblist):
            if gitInfo:
                self._gitList.append(gitInfo)

        for gitInfo in pool.imap(applyJob, otherJoblist):
            if gitInfo:
                self._gitList.append(gitInfo)

    def findGitUrl(self):
        print("[{name}] Searching for PKGBUILD for package (basename: {base}) - {version}".format(**self._info))
        print("[{name}] Generating git urls for package".format(**self._info))

        gitPool = Package._THREAD_POOLS.pop()

        self._genGitList(gitPool)

        fallbackVersion = None
        for gitInfo in self._gitList:
            print("[{name}] Checking url: {}".format(str(gitInfo), **self._info))

            gitDir = tempfile.mkdtemp(prefix = self.base + ".")
            gitCloneArgs = {
                'single_branch': True
            }
            if "branch" in gitInfo:
                gitCloneArgs['branch'] = gitInfo['branch']

            gitRepo = git.Repo.clone_from(gitInfo['url'], gitDir, **gitCloneArgs)
            gitver, githash = self._checkoutToVersion(gitRepo)
            shutil.rmtree(gitDir)

            if gitver:
                gitInfo['hash'] = githash

                if self.version == gitver:
                    self._info.update(gitInfo)
                    break
                elif not fallbackVersion:
                    fallbackVersion = gitInfo.copy()
                    fallbackVersion['version'] = gitver

        if "hash" not in self._info:
            if fallbackVersion:
                self._info.update(fallbackVersion)
            else:
                raise LookupError("[{name}] ERROR: checkout hash not found for version {version}".format(**self._info))

        print("[{name}] Found version ({version}) - hash: {hash}".format(**self._info))
        Package._THREAD_POOLS.append(gitPool)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description = "Download and build manjaro packages")
    parser.add_argument("--deps", help = "Add direct package Depends", action = "store_true")
    parser.add_argument("--ignore-pkgrel", help = "Ignore pkgrel when searching for package", action = "store_true")
    parser.add_argument("packages", help = "List of packages", nargs='+')

    args = parser.parse_args()
    packageNameList = set(args.packages)

    if args.deps:
        for packageName in args.packages:
            packageNameList |= set(Pacman.get_info(packageName)['Depends On'].split())

    thredPollNum = min(os.cpu_count(), len(packageNameList))

    pkgPool = ThreadPool(thredPollNum)
    Package.initThreadPools(thredPollNum)
    Package.genManjaroGroupList(pkgPool)
    packages = Package.genPackateList(packageNameList, pkgPool, args.ignore_pkgrel)

    pkgPool.map(Package.findGitUrl, packages)

    pkgPool.close()

    print("Generating download and build scripts")

    downloadConetent = [ "#!/bin/bash -ex" ]
    buildConetent = [ "#!/bin/bash -ex" ]

    for package in packages:
        gitCloneCmd = "git clone -q {url} . --single-branch"
        if "branch" in package.info():
            gitCloneCmd += " --branch {branch}"

        downloadConetent += [
            "mkdir {base}".format(**package.info()),
            "pushd {base} > /dev/null".format(**package.info()),
            gitCloneCmd.format(**package.info()),
            "git checkout -q {hash}".format(**package.info()),
            "if [ -d trunk ]; then",
            "    popd > /dev/null",
            "    mv {base} {base}.git".format(**package.info()),
            "    mv {base}.git/trunk {base}".format(**package.info()),
            "    rm -rf {base}.git".format(**package.info()),
            "    pushd {base} > /dev/null".format(**package.info()),
            "fi",
            "",
            "makepkg --verifysource --skippgpcheck --nodeps # {base} #".format(**package.info()),
            "popd",
            ""
        ]

        buildConetent += [
            "pushd {base} > /dev/null".format(**package.info()),
            "makepkg --noconfirm --sign --skippgpcheck -irs # {base} #".format(**package.info()),
            "popd > /dev/null",
            ""
        ]

    os.mkdir("build")
    ### Download Script ###
    with open("build/download.sh", "w") as f:
        for line in downloadConetent:
            f.write(line + '\n')

        mode = os.fstat(f.fileno()).st_mode
        mode |= stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH
        os.fchmod(f.fileno(), stat.S_IMODE(mode))

    ### Build Script ###
    with open("build/build.sh", "w") as f:
        for line in buildConetent:
            f.write(line + '\n')

        mode = os.fstat(f.fileno()).st_mode
        mode |= stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH
        os.fchmod(f.fileno(), stat.S_IMODE(mode))
