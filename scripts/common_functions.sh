versionFromPkgbuild() {
    projectName="$1"

    if echo "$projectName" | grep -q -e '-git$' -e '-hg$' && grep -q 'pkgver()' PKGBUILD; then
        makepkg --skippgpcheck --nodeps --verifysource > /dev/null 2>&1
        makepkg --skippgpcheck --nodeps --nobuild > /dev/null 2>&1 # Automatically updates PKGBUILD with new version

        find -mindepth 1 -maxdepth 1 -type d ! \( -name .git -o -name .hg \) -exec rm -rf {} +
    fi

    gitpkgver=$( (cat PKGBUILD; echo; echo 'echo $pkgver') | bash 2> /dev/null)
    gitpkgrel=$( (cat PKGBUILD; echo;echo 'echo $pkgrel') | bash 2> /dev/null)
    gitepoch=$( (cat PKGBUILD; echo;echo 'echo $epoch') | bash 2> /dev/null)

    if [ -z "$gitpkgver" ]; then
        gitpkgver=$(awk -F = '$1 == "pkgver" {print $2}' PKGBUILD)
        gitpkgrel=$(awk -F = '$1 == "pkgrel" {print $2}' PKGBUILD)
        gitpkgver=$(awk -F = '$1 == "epoch" {print $2}' PKGBUILD)
    fi

    gitver="${gitpkgver}-${gitpkgrel}"

    if [ "$gitepoch" ]; then
        gitver="$gitepoch:$gitver"
    fi

    echo "$gitver"
}

namesFromPkgbuild() {
    gitpkgname=$( (cat PKGBUILD; echo; echo 'echo ${pkgname[@]}') | bash 2> /dev/null)

    if [ -z "gitpkgname" ]; then
        gitpkgname=$(awk -F = '$1 == "pkgname" {print $0}' PKGBUILD | cut -d = -f 2- | tr -d ')(')
    fi

    echo "$gitpkgname"
}

checkoutToVersion() {
    argver="$1"
    gitver=""
    projectName=$(basename "$PWD")

    for ((i=0; i < 20; ++i)); do
        if [ -d trunk ]; then
            pushd trunk > /dev/null
        elif [ -e PKGBUILD ]; then
            pushd . > /dev/null
        else
            echo "WARNING: PKGBUILD not found" >&2
            continue
        fi

        gitver="$(versionFromPkgbuild "$projectName")"

        verCheck=$(vercmp "$argver" "$gitver")
        [ "$verCheck" -ge 0 ] && break

        prevHash=$(git log -n 2 --pretty=format:'%H' . | tail -n 1)

        popd > /dev/null

        git checkout -q "$prevHash"
    done

    echo "$gitver"
}
