#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
. "$SCRIPT_DIR/common_functions.sh"

packages=()
withDeps=0

while [ "$1" != "" ]; do
    case $1 in
        --deps)
            withDeps=1
            ;;
        *)
            packages+=($1)
            ;;
    esac
    shift
done

if [ "$withDeps" = "1" ]; then
    for packageName in "${packages[@]}"; do
        packages+=($(LC_ALL=C pacman -Si "$packageName" | awk '$1 == "Depends" {for(i = 4; i <= NF; ++i) print $i; exit}'))
    done
fi

packages=( $(printf '%s\n' "${packages[@]}" | sort -u) )
packagesBase=()
checkoutHashList=()
checkoutUrlList=()
checkoutBranchList=()

for packageName in "${packages[@]}"; do
    packageBaseName=$(curl -s "https://archlinux.org/packages/search/json/?q=$packageName" | jq -r --arg pkgname "$packageName" 'limit(1; .results[] | select(.pkgname == $pkgname).pkgbase)')
    if [ "$packageBaseName" ]; then
        packagesBase+=("$packageBaseName")
    else
        packagesBase+=("$packageName")
    fi
done

echo "Generating Manjaro gitlab group list"
groupListJson="$(curl --silent "https://gitlab.manjaro.org/api/v4/groups")"
groupManjaroList=()
groupList=()

i=1
while [ "$groupListJson" != "[]" ]; do
    groupManjaroList+=($(echo $groupListJson | jq -r '.[] | .full_path' | grep -e '^packages' -e '^security-overlay'))
    groupList+=($(echo $groupListJson | jq -r '.[] | .full_path' | grep -v -e'^manjaro' -e '^packages' -e '^security-overlay'))

    i=$((i+1))
    groupListJson="$(curl --silent "https://gitlab.manjaro.org/api/v4/groups?page=$i")"
done

for (( i=0; i < ${#packages[@]}; ++i )); do
    packageName=${packages[i]}
    packageBaseName=${packagesBase[i]}
    packageVersion=$(LC_ALL=C pacman -Si "$packageName" | awk '$1 == "Version" {print $3}' | tail -n 1)
    echo "[$packageName] Searching for PKGBUILD for package (basename: $packageBaseName) - $packageVersion"

    echo "[$packageName] Generating git urls for package"
    gitUrlList=()
    gitBranchList=()
    for groupName in "${groupManjaroList[@]}"; do
        checkoutUrl="$(echo "$groupName/$packageBaseName" | sed 's;/;%2F;g' | xargs -I {} curl --silent "https://gitlab.manjaro.org/api/v4/projects/{}" | jq -r '.http_url_to_repo' 2> /dev/null)"
        if [ "$checkoutUrl" != "null" ]; then
            gitUrlList+=( "$checkoutUrl" )
            gitBranchList+=("")
        fi
    done

    archGitBranch="packages/$packageBaseName"
    for archGitRepo in svntogit-community svntogit-packages; do
        archGitUrl="https://github.com/archlinux/$archGitRepo.git"
        if git ls-remote -q --exit-code --heads "$archGitUrl" "$archGitBranch" > /dev/null; then
            gitUrlList+=( "$archGitUrl" )
            gitBranchList+=("$archGitBranch")
        fi
    done

    if [ "$FALLBACK_GITLAB_GROUP" ]; then
        checkoutUrl="$(echo "$FALLBACK_GITLAB_GROUP/$packageBaseName" | sed 's;/;%2F;g' | xargs -I {} curl --silent "https://gitlab.com/api/v4/projects/{}" | jq -r '.http_url_to_repo' 2> /dev/null)"
        if [ "$checkoutUrl" != "null" ]; then
            gitUrlList+=( "$checkoutUrl" )
            gitBranchList+=("")
        fi
    fi

    checkoutHash=""
    checkoutUrl=""
    checkoutBranch=""
    for (( j=0; j < ${#gitUrlList[@]}; ++j )); do
        checkoutUrl="${gitUrlList[j]}"
        checkoutBranch="${gitBranchList[j]}"
        echo "[$packageName] Checking url: $checkoutUrl (branch: $checkoutBranch)"

        gitBranchArgs=""
        if [ "$checkoutBranch" ]; then
            gitBranchArgs="--branch $checkoutBranch"
        fi

        gitDir=$(mktemp -d "${packageName}.XXXXXX")
        git clone -q "$checkoutUrl" "$gitDir" --single-branch $gitBranchArgs
        pushd "$gitDir" > /dev/null

        gitver="$(checkoutToVersion "$packageVersion")"

        if [ "$gitver" = "$packageVersion" ]; then
            checkoutHash="$(git log -n 1 --pretty=format:'%H')"
            echo "[$packageName] Found version - hash: $checkoutHash"
        fi

        popd > /dev/null
        rm -fr "$gitDir"

        [ "$checkoutHash" ] && break
    done

    if [ -z "$checkoutHash" ]; then
        echo "[$packageName] ERROR: checkout hash not found for version $packageVersion" >&2
        exit 1
    fi

    checkoutHashList+=("$checkoutHash")
    checkoutUrlList+=("$checkoutUrl")
    checkoutBranchList+=("$checkoutBranch")
done

echo "Generating download and build scripts"
mkdir -p build

### Download Script ###
echo "#!/bin/bash -ex" > build/download.sh

for (( i=0; i < ${#packagesBase[@]}; ++i )); do
    packageName=${packagesBase[i]}
    checkoutUrl=${checkoutUrlList[i]}
    checkoutHash=${checkoutHashList[i]}
    checkoutBranch=${checkoutBranchList[i]}

    gitBranchArgs=""
    if [ "$checkoutBranch" ]; then
        gitBranchArgs="--branch $checkoutBranch"
    fi

    cat >> build/download.sh <<EOF
git clone $checkoutUrl $packageName --single-branch $gitBranchArgs
cd $packageName
git checkout -q $checkoutHash

if [ -d trunk ]; then
    cd ..
    mv $packageName ${packageName}.git
    mv ${packageName}.git/trunk $packageName
    rm -rf ${packageName}.git
    cd $packageName
fi

makepkg --verifysource --skippgpcheck --nodeps # $packageName #
cd ..

EOF
done
chmod +x build/download.sh

### Build Script ###
echo "#!/bin/bash -ex" > build/build.sh
for (( i=0; i < ${#packagesBase[@]}; ++i )); do
    packageName=${packagesBase[i]}

    cat >> build/build.sh <<EOF
cd $packageName
makepkg --noconfirm --sign --skippgpcheck -irs # $packageName
cd ..

EOF
done
chmod +x build/build.sh
